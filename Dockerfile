FROM debian:buster

RUN apt-get update 
RUN	apt-get install -y nginx wget mariadb-server mariadb-client vim 

RUN	apt-get install -y php7.3-fpm php7.3-common php7.3-mysql php7.3-xml php7.3-xmlrpc php7.3-curl php7.3-gd php7.3-imagick php7.3-cli php7.3-dev php7.3-imap php7.3-mbstring php7.3-soap php7.3-zip php7.3-bcmath

RUN wget https://files.phpmyadmin.net/phpMyAdmin/5.0.1/phpMyAdmin-5.0.1-english.tar.gz
RUN	tar -xzf phpMyAdmin-5.0.1-english.tar.gz -C /var/www/html
RUN mv /var/www/html/phpMyAdmin-5.0.1-english /var/www/html/phpmyadmin

RUN wget https://fr.wordpress.org/latest-fr_FR.tar.gz
RUN tar -xzf latest-fr_FR.tar.gz -C /var/www/html

RUN chown -R www-data:www-data /var/www/html
RUN chmod -R 755 /var/www/html


ADD ./srcs/default /etc/nginx/sites-available/default
ADD ./srcs/nginx-selfsigned.crt	/etc/ssl/certs/nginx-selfsigned.crt
ADD ./srcs/nginx-selfsigned.key	/etc/ssl/private/nginx-selfsigned.key
ADD ./srcs/wp-config.php /var/www/localhost/wordpress
ADD ./srcs/script.sh /

EXPOSE 80
EXPOSE 443

CMD bash script.sh
