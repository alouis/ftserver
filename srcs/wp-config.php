<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'wordpress');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'wordpressuser');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'password');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm=w)!7{-EEc&JYU~$wd@jTrqFseaZ0D-4Vd/?!>_hcF*BmQ+S2Do!QP>>O-|OI21');
define('SECURE_AUTH_KEY',  'S?lk-{RG 5K~sd*1$N<aZ18jy|^0n#-@eGqBhk3#dJy2M-|jUruu[T+ cYfJ^@2-');
define('LOGGED_IN_KEY', '>i*8?IA#h/.@?6MezjmoBWm&&b+h1YP?T.]Y=&*^h9[Bm`ThdbJ5zepb824LUd;-');
define('NONCE_KEY',        'cPim1L6}H1rQLtLj|FrN1DO:LZVsh`rr}5 `}k,f~%u)papX4|_J^Q%PKJ44uF[l');
define('AUTH_SALT',        ',+Aa_iZ/%yj5?-0F.O>Ogd6jCLU+2_2M$+1Zo-hUog70lLa$)YI@wbzkN<~v!Acd');
define('SECURE_AUTH_SALT', '-9sQ8iLS}1-iEX)b<A6(JNuPIGv2SV5ZiHV])4i+@oi6FG76$4{A@c*fj8[ *Uc-');
define('LOGGED_IN_SALT', 'K$i5b^g?TK4M|w;mqlh>m9ZJ5eVAq0X;we}jvw:JNkKm-O|-=GdH-{I><`J(ZgKB');
define('NONCE_SALT', 'c_VY?z=E}2r0A&r!F/qk*rtM3>K-Id+z*qG*^2g#4/-sR2%GP>b|{<97nL4uP8K/');

/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
